from projects.views import show_project, show_project_details, create_project
from django.urls import path


urlpatterns = [
    path("", show_project, name="list_projects"),
    path("<int:id>/", show_project_details, name="show_project"),
    path("create/", create_project, name="create_project"),
]
